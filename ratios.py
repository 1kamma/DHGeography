import pandas as pd
import dash
from dash import dcc, html
from dash.dependencies import Input, Output
import plotly.graph_objects as go

# Read the CSV file into a Pandas DataFrame
file_path = "texts_by_period_and_location_saparated_by_periods_in_columns.csv"
df = pd.read_csv(file_path)

# Initialize the Dash app
app = dash.Dash(__name__)

# Define the columns to summarize
columns_to_summarize = ["ancient", "old", "middle", "new", "late"]

# Create the figure for the initial graph (placeholder data)
initial_data = df.groupby("place")[columns_to_summarize].sum().reset_index()
initial_fig = go.Figure()
initial_fig.layout.height = 700  # You can set the desired height here
for col in columns_to_summarize:
    initial_fig.add_trace(
        go.Bar(
            name=col, x=initial_data["place"],
            y=initial_data[col],
            text=initial_data[col],
            textposition="auto"))

# Define the app layout
app.layout = html.Div([
    html.H1("Dynamic Stacked Bar Graph with Labels"),
    dcc.Dropdown(
        id="place-selector",
        options=[{"label": place, "value": place} for place in df["place"].unique()],
        multi=True,
        placeholder="Select Places",
    ),
    dcc.Graph(id="bar-graph", figure=initial_fig),
])

# Define a callback to update the graph based on selected places


@app.callback(
    Output("bar-graph", "figure"),
    [Input("place-selector", "value")]
)
def update_graph(selected_places):
    filtered_df = df[df["place"].isin(selected_places)]
    grouped = filtered_df.groupby("place")[columns_to_summarize].sum().reset_index()

    fig = go.Figure()

    for col in columns_to_summarize:
        fig.add_trace(go.Bar(name=col, x=grouped["place"], y=grouped[col], text=grouped[col], textposition="auto"))

    fig.update_layout(barmode="stack")  # Set the bars to be stacked

    for index, place in enumerate(grouped["place"]):
        total = grouped[columns_to_summarize].iloc[index].sum()
        fig.add_annotation(text=f"Total: {total}", x=place, y=total + 5)

    # Adjust the height of the plot area
    fig.update_layout(height=700)  # You can set the desired height here

    return fig


if __name__ == "__main__":
    app.run_server(debug=True)
